#! /bin/sh
#
# entrypoint.sh
# Copyright (C) 2018 Milan Jovanovic <m.jovanovic.rs@gmail.com>
#
# Distributed under terms of the MIT license.
#

set -e

rm -f /var/run/apache2/apache2.pid

exec /usr/sbin/apache2ctl -DFOREGROUND
